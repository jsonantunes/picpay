<?php

namespace App\Enum;

class UserAccountEnum
{
    const CONSUMER_USER_ACCOUNT = 1;
    const SELLER_USER_ACCOUNT = 2;
}