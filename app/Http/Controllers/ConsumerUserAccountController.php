<?php

namespace App\Http\Controllers;

use App\Http\Validation\UserAccount\CreateConsumerUserAccountValidation;
use App\Services\ConsumerUserAccountService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ConsumerUserAccountController extends Controller
{
    protected $service;

    public function __construct(ConsumerUserAccountService $service)
    {
        $this->service = $service;
    }

    public function store(): JsonResponse
    {
        $validation = app(CreateConsumerUserAccountValidation::class);
        $newConsumerUserAccount = $this->service->create($validation);

        return response()->json($newConsumerUserAccount, Response::HTTP_CREATED);
    }
}