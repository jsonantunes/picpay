<?php

namespace App\Http\Controllers;

use App\Http\Validation\UserAccount\CreateSellerUserAccountValidation;
use Illuminate\Http\JsonResponse;
use App\Services\SellerUserAccountService;
use Symfony\Component\HttpFoundation\Response;

class SellerUserAccountController extends Controller
{
    protected $service;

    public function __construct(SellerUserAccountService $service)
    {
        $this->service = $service;
    }

    public function store(): JsonResponse
    {
        $validation = app(CreateSellerUserAccountValidation::class);
        $newSellerUserAccount = $this->service->create($validation);

        return response()->json($newSellerUserAccount, Response::HTTP_CREATED);
    }
}