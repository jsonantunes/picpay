<?php

namespace App\Http\Controllers;

use App\Http\Validation\Transaction\CreateTransactionValidation;
use App\Services\TransactionService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TransactionController extends Controller
{
    protected $service;

    public function __construct(TransactionService $service)
    {
        $this->service = $service;
    }

    public function store(): JsonResponse
    {
        $validation = app(CreateTransactionValidation::class);
        $transaction = $this->service->create($validation);

        return response()->json($transaction, Response::HTTP_CREATED);
    }

    public function show($transactionId): JsonResponse
    {
        $transaction = $this->service->findById($transactionId);

        return response()->json($transaction);
    }
}