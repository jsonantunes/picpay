<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Http\Validation\User\CreateUserValidation;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;
use App\Services\UserService;

class UserController extends Controller
{
    protected $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function store(): JsonResponse
    {
        $validation = app(CreateUserValidation::class);
        $user = $this->service->create($validation);

        return response()->json($user, Response::HTTP_CREATED);
    }

    public function index()
    {
        $users = $this->service->listByNameOrUsername();

        return response()->json($users);
    }

    public function show($userId)
    {
        $user = $this->service->findById($userId);

        return response()->json($user);
    }
}