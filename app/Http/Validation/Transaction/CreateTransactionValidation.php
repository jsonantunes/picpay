<?php

namespace App\Http\Validation\Transaction;

use App\Http\Validation\Validation;

class CreateTransactionValidation extends Validation
{
    public function rules()
    {
        return [
            'payee_id' => ['required', 'exists:users_accounts,id'],
            'payer_id' => ['required', 'exists:users_accounts,id'],
            'value' => ['required', 'numeric'],
        ];
    }
}