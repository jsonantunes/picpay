<?php

namespace App\Http\Validation\User;

use App\Http\Validation\Validation;

class CreateUserValidation extends Validation
{
    public function rules()
    {
        return [
            'full_name' => 'required',
            'cpf' => ['required', 'digits:11', 'unique:users'],
            'phone_number' => 'required',
            'email' => ['required', 'email', 'unique:users'],
            'password' => 'required',
        ];
    }
}