<?php

namespace App\Http\Validation\UserAccount;

use App\Http\Validation\Validation;
use Illuminate\Validation\Rule;

abstract class AbstractCreateUserAccountValidation extends Validation
{
    protected function getUniqueUserAccountRule(int $accountId)
    {
        return Rule::unique('users_accounts')->where(function ($query) use ($accountId) {
            $userId = $this->request->input('user_id');
            return $query->where('user_id', $userId)->where('account_id', $accountId);
        });
    }
}