<?php

namespace App\Http\Validation\UserAccount;

use App\Enum\UserAccountEnum;

class CreateConsumerUserAccountValidation extends AbstractCreateUserAccountValidation
{
    public function rules()
    {
        $uniqueAccountRule = $this->getUniqueUserAccountRule(UserAccountEnum::CONSUMER_USER_ACCOUNT);

        return [
            'user_id' => [
                'required',
                'exists:users,id',
                $uniqueAccountRule
            ],
            'username' => ['required', 'unique:users_accounts'],
        ];
    }
}