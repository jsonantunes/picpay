<?php

namespace App\Http\Validation\UserAccount;

use App\Enum\UserAccountEnum;

class CreateSellerUserAccountValidation extends AbstractCreateUserAccountValidation
{
    public function rules()
    {
        $uniqueAccountRule = $this->getUniqueUserAccountRule(UserAccountEnum::SELLER_USER_ACCOUNT);

        return [
            'user_id' => [
                'required',
                'exists:users,id',
                $uniqueAccountRule
            ],
            'username' => ['required', 'unique:users_accounts'],
            'cnpj' => 'required',
            'fantasy_name' => 'required',
            'social_name' => 'required'
        ];
    }
}

