<?php

namespace App\Http\Validation;

use App\Http\ResponseValidation;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;
use Symfony\Component\HttpFoundation\Response;

abstract class Validation
{
    use ProvidesConvenienceMethods {
        validate as rulesValidate;
    }

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function validate(): void
    {
        $this->rulesValidate($this->request, $this->rules());
    }

    public abstract function rules();
}