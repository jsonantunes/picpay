<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = [
        'user_account_id', 'cnpj', 'fantasy_name', 'social_name',
    ];

    public $hidden = ['user_account_id'];

    public $timestamps = false;

    public function userAccount()
    {
        return $this->hasOne('App\Models\UserAccount');
    }
}