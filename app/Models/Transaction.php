<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const UPDATED_AT = null;
    const CREATED_AT = 'transaction_date';

    public $fillable = [
        'payer_id', 'payee_id', 'value',
    ];
}