<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'full_name', 'cpf', 'phone_number', 'email', 'password',
    ];

    public $timestamps = false;

    public function accounts()
    {
        return $this->hasMany('App\Models\UserAccount');
    }
}
