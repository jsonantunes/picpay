<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    protected $table = 'users_accounts';

    protected $fillable = [
        'user_id', 'account_id', 'username',
    ];

    protected $hidden = ['account_id'];

    public $timestamps = false;

    public function seller()
    {
        return $this->hasOne('App\Models\Seller');
    }
}