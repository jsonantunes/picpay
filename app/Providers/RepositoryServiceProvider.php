<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\UserRepositoryInterface',
            'App\Repositories\UserRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ConsumerUserAccountRepositoryInterface',
            'App\Repositories\ConsumerUserAccountRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\SellerUserAccountRepositoryInterface',
            'App\Repositories\SellerUserAccountRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\TransactionRepositoryInterface',
            'App\Repositories\TransactionRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\TransactionAuthorizateRepositoryInterface',
            'App\Repositories\TransactionAuthorizateRepository'
        );
    }
}
