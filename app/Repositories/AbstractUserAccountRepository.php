<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contracts\UserAccountRepositoryInterface;

abstract class AbstractUserAccountRepository implements UserAccountRepositoryInterface
{
    protected $model;
    protected $fields;
    protected $userAccount;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public abstract function create(array $fields): \stdClass;

    protected function createUserAccount(int $accountId): void
    {
        $this->fields['account_id'] = $accountId;

        $query = $this->model->newQuery();
        $user = $query->find($this->fields['user_id']);

        $userAccountModel = $user->accounts();
        $this->userAccount = $userAccountModel->create($this->fields);
    }
}