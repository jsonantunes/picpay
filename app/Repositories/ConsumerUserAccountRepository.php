<?php

namespace App\Repositories;

use App\Enum\UserAccountEnum;

class ConsumerUserAccountRepository extends AbstractUserAccountRepository
{
    public function create(array $fields): \stdClass
    {
        $this->fields = $fields;
        $this->createUserAccount(UserAccountEnum::CONSUMER_USER_ACCOUNT);

        return (object) $this->userAccount->toArray();
    }
}