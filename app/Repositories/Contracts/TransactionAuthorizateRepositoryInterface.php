<?php

namespace App\Repositories\Contracts;

interface TransactionAuthorizateRepositoryInterface
{
    public function authorize(float $value): bool;
}