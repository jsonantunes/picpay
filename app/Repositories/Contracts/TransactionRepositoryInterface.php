<?php

namespace App\Repositories\Contracts;

interface TransactionRepositoryInterface
{
    public function create(array $fields): \stdClass;
    public function findById(int $transactionId): \stdClass;
}