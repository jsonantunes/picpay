<?php


namespace App\Repositories\Contracts;


interface UserAccountRepositoryInterface
{
    public function create(array $fields): \stdClass;
}