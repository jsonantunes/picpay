<?php

namespace App\Repositories\Contracts;

use App\Models\User;

interface UserRepositoryInterface
{
    public function __construct(User $user);
    public function create(array $userFillable): \stdClass;
    public function listByNameOrUsername(string $nameOrUsername): array;
    public function findById(int $id): \stdClass;
}