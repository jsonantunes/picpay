<?php

namespace App\Repositories;

use App\Enum\UserAccountEnum;
use Illuminate\Support\Facades\DB;

class SellerUserAccountRepository extends AbstractUserAccountRepository
{
    private $sellerUserAccount;

    public function create(array $fields): \stdClass
    {
        $this->fields = $fields;

        DB::transaction(function () {
            $this->createUserAccount(UserAccountEnum::SELLER_USER_ACCOUNT);
            $this->createSellerUserAccount();
        });

        return $this->getSellerUserAccountEntity();
    }

    private function createSellerUserAccount(): void
    {
        $sellerUserAccount = $this->userAccount->seller();
        $this->sellerUserAccount = $sellerUserAccount->create($this->fields);
    }

    private function getSellerUserAccountEntity(): \stdClass
    {
        $sellerUserAccountEntity = (object) $this->sellerUserAccount->toArray();

        $sellerUserAccountEntity->id = $this->userAccount->id;
        $sellerUserAccountEntity->user_id = $this->userAccount->user_id;
        $sellerUserAccountEntity->username = $this->userAccount->username;

        return $sellerUserAccountEntity;
    }
}