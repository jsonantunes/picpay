<?php

namespace App\Repositories;

use App\Repositories\Contracts\TransactionAuthorizateRepositoryInterface;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class TransactionAuthorizateRepository implements TransactionAuthorizateRepositoryInterface
{
    const API_AUTHORIZATION = 'http://users-api-php/mockup/authorizate';

    protected $httpClient;

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function authorize(float $value): bool
    {
        try {
            return $this->allowTransaction($value);
        } catch (\Exception $exception) {
            return false;
        }
    }

    private function allowTransaction(float $value): bool
    {
        $requestOptions = ['json' => ['value' => $value]];
        $response = $this->httpClient->request('POST', self::API_AUTHORIZATION, $requestOptions);

        return $response->getStatusCode() == Response::HTTP_OK;
    }
}