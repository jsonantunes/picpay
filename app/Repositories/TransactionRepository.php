<?php

namespace App\Repositories;

use App\Models\Transaction;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use Carbon\Carbon;

class TransactionRepository implements TransactionRepositoryInterface
{
    protected $model;

    public function __construct(Transaction $transactionModel)
    {
        $this->model = $transactionModel;
    }

    public function create(array $fields): \stdClass
    {
        $transactionModel = $this->model->create($fields);

        $transactionEntity = (object) $transactionModel->toArray();
        $transactionEntity->transaction_date = $this->formatTransactionDate($transactionEntity->transaction_date);

        return $transactionEntity;
    }

    private function formatTransactionDate(string $transactionDate)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $transactionDate)->format('Y-m-d\TH:i:s.u\Z');
    }

    public function findById(int $id): \stdClass
    {
        $transaction = $this->model->findOrFail($id);

        $transactionEntity = (object) $transaction->toArray();
        $transactionEntity->value = (float) number_format($transactionEntity->value, 2);
        $transactionEntity->transaction_date = $this->formatTransactionDate($transactionEntity->transaction_date);

        return $transactionEntity;
    }
}