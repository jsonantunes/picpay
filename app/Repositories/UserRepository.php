<?php

namespace App\Repositories;

use App\Enum\UserAccountEnum;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function create(array $fields): \stdClass
    {
        $fields['password'] = Hash::make($fields['password']);
        $newUser = $this->model->create($fields);

        return (object)$newUser->toArray();
    }

    public function listByNameOrUsername(string $nameOrUsername): array
    {
        $query = $this->model->newQuery();

        $query->distinct();
        $query->select('users.*');

        $this->applyFilterByNameOrUsername($query, $nameOrUsername);

        $users = $query->get();
        return $users->toArray();
    }

    private function applyFilterByNameOrUsername(Builder $query, string $nameOrUsername): void
    {
        if ($this->allowFilterByNameOrUsername($nameOrUsername)) {
            $query->leftJoin('users_accounts', 'users.id', '=', 'users_accounts.user_id');
            $query->where('users.full_name', 'like', $nameOrUsername . '%');
            $query->orWhere('users_accounts.username', 'like', $nameOrUsername . '%');
        }
    }

    private function allowFilterByNameOrUsername(string $nameOrUsername): bool
    {
        return !empty($nameOrUsername) && strpos($nameOrUsername, 'user') === false;
    }

    public function findById(int $id): \stdClass
    {
        $user = $this->model->findOrFail($id);

        $userEntity = new \stdClass();
        $userEntity->user = (object)$user->toArray();

        $userEntity->accounts = new \stdClass();
        $userEntity->accounts->seller = $this->getSellerUserAccount($user);
        $userEntity->accounts->consumer = $this->getCustomerUserAccount($user);

        return $userEntity;

    }

    private function getSellerUserAccount($user): \stdClass
    {
        $seller = $user->accounts()
            ->with('seller')
            ->where('account_id', UserAccountEnum::SELLER_USER_ACCOUNT)
            ->first();

        if (empty($seller))
            return new \stdClass();

        $sellerEntity = new \stdClass();
        $sellerEntity->id = $seller->id;
        $sellerEntity->user_id = $seller->user_id;
        $sellerEntity->username = $seller->username;
        $sellerEntity->cnpj = $seller->seller->cnpj;
        $sellerEntity->fantasy_name = $seller->seller->fantasy_name;
        $sellerEntity->social_name = $seller->seller->social_name;

        return $sellerEntity;
    }

    private function getCustomerUserAccount($user): \stdClass
    {
        $customer = $user->accounts()
            ->where('account_id', UserAccountEnum::CONSUMER_USER_ACCOUNT)
            ->first();

        if (empty($customer))
            return new \stdClass();

        return (object)$customer->toArray();
    }
}