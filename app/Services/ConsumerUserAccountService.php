<?php

namespace App\Services;

use App\Http\Validation\UserAccount\CreateConsumerUserAccountValidation;
use App\Repositories\ConsumerUserAccountRepository;
use Illuminate\Http\Request;

class ConsumerUserAccountService
{
    protected $request;
    protected $repository;

    public function __construct(Request $request, ConsumerUserAccountRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    public function create(CreateConsumerUserAccountValidation $validation): \stdClass
    {
        $validation->validate();
        $fields = $this->request->all();

        return $this->repository->create($fields);
    }
}