<?php

namespace App\Services;

use App\Http\Validation\UserAccount\CreateSellerUserAccountValidation;
use App\Repositories\SellerUserAccountRepository;
use Illuminate\Http\Request;

class SellerUserAccountService
{
    protected $repository;
    protected $request;

    public function __construct(Request $request, SellerUserAccountRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    public function create(CreateSellerUserAccountValidation $validation): \stdClass
    {
        $validation->validate();
        $fields = $this->request->all();

        return $this->repository->create($fields);
    }
}