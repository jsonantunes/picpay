<?php


namespace App\Services;


use App\Http\Validation\Transaction\CreateTransactionValidation;
use App\Repositories\Contracts\TransactionAuthorizateRepositoryInterface;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TransactionService
{
    protected $transactionRepository;
    protected $transactionAuthorizateRepository;
    protected $request;

    public function __construct()
    {
        $this->transactionRepository = app(TransactionRepositoryInterface::class);
        $this->transactionAuthorizateRepository = app(TransactionAuthorizateRepositoryInterface::class);
        $this->request = app(Request::class);
    }

    public function create(CreateTransactionValidation $validation): \stdClass
    {
        $validation->validate();
        $fields = $this->request->all();

        if (!$this->transactionAuthorizateRepository->authorize($fields['value']))
            throw new HttpException(Response::HTTP_UNAUTHORIZED);

        return $this->transactionRepository->create($fields);
    }

    public function findById(int $transactionId): \stdClass
    {
        return $this->transactionRepository->findById($transactionId);
    }
}