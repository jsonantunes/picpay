<?php

namespace App\Services;

use App\Http\Validation\User\CreateUserValidation;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserService
{
    protected $request;
    protected $repository;

    public function __construct(Request $request, UserRepositoryInterface $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    public function create(CreateUserValidation $validation): \stdClass
    {
        $validation->validate();
        $fields = $this->request->all();

        return $this->repository->create($fields);
    }

    public function listByNameOrUsername(): array
    {
        $nameOrUsername = $this->request->query('q');

        return $this->repository->listByNameOrUsername($nameOrUsername);
    }

    public function findById(int $userId): \stdClass
    {
        return $this->repository->findById($userId);
    }
}