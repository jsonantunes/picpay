<?php

namespace Mockup\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthorizationController extends Controller
{
    const MAX_AUTHORIZATION_VALUE = 100;

    public function index(Request $request)
    {
        $transactionValue = $request->input('value');

        if ($transactionValue >= self::MAX_AUTHORIZATION_VALUE)
            throw new HttpException(Response::HTTP_UNAUTHORIZED);
    }
}