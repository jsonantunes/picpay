<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('users/{user_id}', ['uses' => 'UserController@show']);
$router->post('users', ['uses' => 'UserController@store']);
$router->post('users/consumers', ['uses' => 'ConsumerUserAccountController@store']);
$router->post('users/sellers', ['uses' => 'SellerUserAccountController@store']);
$router->get('users', ['uses' => 'UserController@index']);

$router->get('transactions/{transaction_id}', ['uses' => 'TransactionController@show']);
$router->post('transactions', ['uses' => 'TransactionController@store']);

