CREATE SCHEMA IF NOT EXISTS `picpay-backend` DEFAULT CHARACTER SET utf8 ;

USE `picpay-backend`;

-- Adicione a criação das tabelas necessárias para a sua aplicação.
-- Você também pode criar a estrutura do seu banco utilizando as migrations do Lumen.
--
-- Este arquivo será executado no startup do container do MySQL.

CREATE TABLE IF NOT EXISTS `picpay-backend`.`accounts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `picpay-backend`.`accounts` (`id`, `name`) VALUES ('1', 'Customer');
INSERT INTO `picpay-backend`.`accounts` (`id`, `name`) VALUES ('2', 'Seller');

CREATE TABLE IF NOT EXISTS `picpay-backend`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `full_name` VARCHAR(150) NOT NULL,
  `cpf` CHAR(11) NOT NULL,
  `phone_number` VARCHAR(20) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unique_cpf` (`cpf` ASC) ,
  UNIQUE INDEX `unique_email` (`email` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `picpay-backend`.`users_accounts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `account_id` INT(11) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unique_username` (`username` ASC) ,
  UNIQUE INDEX `unique_user_id_and_account_id` (`user_id` ASC, `account_id` ASC) ,
  INDEX `index_account_id` (`account_id` ASC) ,
  INDEX `index_user_id` (`user_id` ASC) ,
  CONSTRAINT `account_id`
    FOREIGN KEY (`account_id`)
    REFERENCES `picpay-backend`.`accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `picpay-backend`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `picpay-backend`.`sellers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_account_id` INT(11) NOT NULL,
  `cnpj` VARCHAR(20) NOT NULL,
  `fantasy_name` VARCHAR(45) NOT NULL,
  `social_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `index_user_account_id` (`user_account_id` ASC) ,
  CONSTRAINT `user_account_id`
    FOREIGN KEY (`user_account_id`)
    REFERENCES `picpay-backend`.`users_accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `picpay-backend`.`transactions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `payee_id` INT(11) NOT NULL,
  `payer_id` INT(11) NOT NULL,
  `value` DECIMAL(10,2) NOT NULL,
  `transaction_date` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `index_payee_id` (`payee_id` ASC) ,
  INDEX `index_payer_id` (`payer_id` ASC) ,
  CONSTRAINT `payee_id`
    FOREIGN KEY (`payee_id`)
    REFERENCES `picpay-backend`.`users_accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `payer_id`
    FOREIGN KEY (`payer_id`)
    REFERENCES `picpay-backend`.`users_accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;